#!/bin/bash

# Start NanoMQ in the background
nanomq start --conf /home/hunter/hunter_workingdir/nanomq.conf &
echo "MQTT broker started"

sleep 5

# Set environment variables
export PYTHONUNBUFFERED=1
export PYTHONPATH=$PYTHONPATH:/home/hunter/develop_hunter/hunter

export HUNTER_MP_SERVER=hunterctrl-vanosten.northeurope.cloudapp.azure.com

# run hunter
/home/hunter/bin/venv/bin/python3 /home/hunter/develop_hunter/hunter/start_controller.py -d /home/hunter/develop_hunter/hunter-scenarios -i OPFOR -x -o 0_0_0_0_0 -g -s test &

echo "Hunter Controller started"
