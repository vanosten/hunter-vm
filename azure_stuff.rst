
================================
Azure Installation
================================

------------------------------
Azure for web-app and database
------------------------------

Everything in this chapter is just a reminder for the maintainer of ``Hunter`` how to setup things in Azure. Your milage might be different.

..............................
Install the Azure CLI on Linux
..............................

Follow the `Azure instructions <https://learn.microsoft.com/en-us/cli/azure/install-azure-cli-linux?pivots=apt>`_, which will installs the `Azure Command-Line Interface (CLI) <https://learn.microsoft.com/en-us/cli/azure/>`_.

To upgrade an existing installation::

    $ az upgrade

.....
Login
.....

Unless you have a Service Principal, then do the following and login through a Browser authentication session::

    $ az login


Check the subscriptions::

    $ az account show --output table


If needed set the correct subscription if there is more than one (where you choose the ``<subscription_id>`` from the list gotten with the previous command - it looks like ``aaf666b-xxxx-yyyy-zzzz-429b2c2349bb``)::

    $ az account set --subscription <subscription_id>


................
Default Location
................

To get a list of all supported locations use ``az account list-locations --query '[].name'``.

We will use ``westeurope``, because it is a good compromise in terms of latency for people in Europe and it is close to the server running Hunter (Ionos). And: it has the majority of all Azure services.

To set it as default::

    $ az config set defaults.location=westeurope


.........................
Create the Resource Group
.........................

All resources run within the same resource group ``hunter``::

    $ az group create -l westeurope -n hunter


...................
Create the Database
...................

The following creates the account, database and container. TTL (time to live) is set to 30 days (2592000 seconds)::

    $ export location="westeurope"
    $ export account="hunter-nosql"
    $ export database="hunter-core"
    $ export resourcegroup="hunter"
    $ export appregistrationname="hunter-nosql-client"

    $ az cosmosdb create -n $account -g $resourcegroup --enable-free-tier true --backup-policy-type Periodic --backup-redundancy Zone --kind GlobalDocumentDB --locations regionName=$location --tags hunter=database

    $ az cosmosdb sql database create -a $account -g $resourcegroup -n $database

    $ az cosmosdb sql container create -a $account -d $database -g $resourcegroup -n sessions -p "/session_id" --ttl 2592000

Now do the service principal stuff (once for each client - cf. `Configure role-based access control with Microsoft Entra ID for your Azure Cosmos DB account <https://learn.microsoft.com/en-gb/azure/cosmos-db/how-to-setup-rbac>`_ -- the existing principals can be found in Entra->Enterprise Applications -> All applications -> App registrations)::

    $ export principal_name="hunter-nosql-webui"
    $ az ad app create --display-name $principal_name

Note down the ``appId`` (looks like "761ee669-5ebb-4d6a-bbd0-........") from the returned JSON - max end data is 24 months from now::

    $ export appid="731d9c33-261b-4b5b-9ff7-5ab7c83ac654"
    $ az ad sp create --id $appid

Note down the id. Then::

    $ az ad app credential reset --id $appid --end-date 2026-02-09

Note down the password and tenant etc. returned. Then::

    $ export spid="7c824428-d407-4c18-a...."

Get a list over roles::

    $ az cosmosdb sql role definition list --account-name $account --resource-group $resourcegroup

Then make the role assignment (use either built-in "Cosmos DB Built-in Data Contributor" or "Cosmos DB Built-in Data Reader")::

    $ roleid="/subscriptions/.../resourceGroups/hunter/providers/Microsoft.DocumentDB/databaseAccounts/hunter-nosql/sqlRoleDefinitions/00000000-0000-0000-0000-000000000002"
    $ az cosmosdb sql role assignment create --account-name $account --resource-group $resourcegroup --scope "/" --principal-id $spid --role-definition-id $roleid

NB: it takes a few minutes for the role assignment to propagate.

...................
App Service Web App
...................

Follow the following steps in the `Azure Portal <https://portal.azure.com>`_ to create a service:

* Select ``App Service`` and then ``Web App``.
* Select ``code`` as publishing model and a ``Python 3.11`` runtime stack on App Plan ``Linux`` in location ``westeurope``.
* Use a ``Free (F1)`` SKU and size.
* Keep ``Public access`` enabled.


Then `App settings <https://learn.microsoft.com/en-gb/azure/app-service/configure-common?tabs=portal>`_ need to be configured. We only need to add the following environment variables for the service principal to connect to Cosmos DB:

* AZ_COSMOS_ACCOUNT_URI
* AZ_COSMOS_CLIENT_ID
* AZ_COSMOS_CLIENT_SECRET
* AZ_COSMOS_TENANT_ID

.............................
Custom script for app startup
.............................

We need a `custom startup command <https://learn.microsoft.com/en-us/azure/app-service/configure-language-python#customize-startup-command>`_ given the package structure for the web-app: ``gunicorn --bind=0.0.0.0 --timeout 600 hunter.web_ui:app``.

.................................
Deployment of new web-ui versions
.................................

We are using the `Local Git deployment <https://learn.microsoft.com/en-gb/azure/app-service/deploy-local-git?tabs=cli>`_ despite this being the production app.

Set up the remote in Azure (only once)::

    $ git remote add azure https://hunter-web-ui.scm.azurewebsites.net:443/hunter-web-ui.git


Push changes to Azure (also pushes stuff not really needed like \*.rst files - but it does not matter too much). The local Git username and the password can be found in the Azure Portal in the App Service Deployment Center::

    $ git push azure master


--------------------------
Azure Application Insights
--------------------------

First create a `Log Analytics Workspace <https://learn.microsoft.com/en-us/azure/azure-monitor/logs/quick-create-workspace?tabs=azure-portal>`_ and then an `Application Insights resource <https://learn.microsoft.com/en-us/azure/azure-monitor/app/create-workspace-resource?tabs=portal#create-an-application-insights-resource>`_ through the portal.


--------
Azure VM
--------

.................
Prepare Terraform
.................

To authenticate Terraform CLI to Azure `using a service principle with secret <https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/guides/service_principal_client_secret>`_ run a command like the following::

    $ az ad sp create-for-rbac --display-name="terraform-cli" --role="Contributor" --scopes="/subscriptions/xxx"

where the subscription ID is from the ``az login`` result.

Save the credential information as environment variables::

    ARM_CLIENT_ID="00000000-0000-0000-0000-000000000000"
    ARM_CLIENT_SECRET="12345678-0000-0000-0000-000000000000"
    ARM_TENANT_ID="10000000-0000-0000-0000-000000000000"
    ARM_SUBSCRIPTION_ID="20000000-0000-0000-0000-000000000000"

............................
Prepare a SSH key for the VM
............................

Run the following commands to create the private and public keys. The name of the key must be the same as referenced in the Terraform script::

    $ ssh-keygen -t rsa -b 4096 -m PEM -f hunter-vm-001_id_rsa


..................
Prepare cloud-init
..................

Statically test the cloud-init configuration as follows::

    $ cloud-init schema -c vm-cloud-init.yml --annotate


............................
Run Terraform to Create a VM
............................

If not done before or change of versions for Terraform/Azurerm, then issue a ``terraform init`` to initialize the ``iac`` (infrastructure as code) directory.

Double check ``variables.tf`` (e.g. you might want to use a different region) and at least change the variable ``domain_name_host_part`` so there are no conflicts in case you run in the same region as the author.

Then use ``terraform plan -var-file="secrets.tfvars"`` and finally ``terraform apply -var-file="secrets.tfvars"``. The ``secrets.tfvars`` contains the ``hunter_password`` in a file, which is not checked in.

.. warning::
    You need to let the VM run for a few minutes to let Ansible finalize downloading system packages as well as Hunter related repositories' content etc.

The output will include the fully qualified domain name (FQDN) for the virtual machine. Otherwise, use ``terraform state list`` and then ``terraform state show azurerm_public_ip.pip-hunterctrl-vm-001`` to get the FQDN for the public IP of the VM (or look it up in the Azure Portal). Having this, you can then connect to the VM::

    $ ssh hunter@hunterctrl-vanosten.northeurope.cloudapp.azure.com

NB: Depending on the setup of your host you might get an error because the fingerprint of the VM has changed and therefore ``ssh`` does not continue. This is the case if you have destroyed the infrastructure and afterwards re-applied. While the FGDN will stay the same for the VM, the fingerprint will not. Consequently, you might need to do something like the following::

    $ ssh-keygen -f "/home/linux/.ssh/known_hosts" -R "hunterctrl-vanosten.northeurope.cloudapp.azure.com"
.....
Debug
.....

If the machine does not look like expected, then start with ``sudo cloud-init status --long`` and afterwards `cloud-init debug <https://cloudinit.readthedocs.io/en/latest/howto/debugging.html>`_ instructions.

...................
Desktop Environment
...................

No desktop environment is needed to run the MP server and the Hunter controller in the virtual machine. However, if you want a UI, then you can login to the VM and change to ``~/develop_hunter/hunter-vm``::

    $ ansible-playbook ansible_playbooks/desktop_stuff.yml --ask-become-pass

Afterwards the ``xrdp`` service must be started and enabled::

    $ sudo systemctl enable xrdp
    $ sudo systemctl start xrdp
    $ sudo systemctl status xrdp

This results in a Xfce4 desktop environment, which can be access through RDP (e.g. using the `KDE Remote Desktop Client KRDC <https://apps.kde.org/en-gb/krdc/>`_ on Linux or the Windows built-in RDP client).
