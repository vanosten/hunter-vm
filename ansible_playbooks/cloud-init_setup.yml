---
- name: Setup the server services
  hosts: localhost
  become: yes
  vars:
    hunter_home_dir: /home/hunter
    hunter_working_dir: /home/hunter/hunter_workingdir
    bin_dir: /home/hunter/bin
    repos_dir: /home/hunter/develop_hunter
    src_fgms_dir: /home/hunter/bin/fgms_src
    build_fgms_dir: /home/hunter/bin/build_fgms
  tasks:
    - name: Ensure Hunter working directory exists
      ansible.builtin.file:
        path: "{{ hunter_working_dir }}/settings"
        state: directory
        owner: hunter
        group: hunter

    # FGMS stuff - the FlightGear Multiplayer Server
    # https://wiki.flightgear.org/Howto:Set_up_a_multiplayer_server
    # https://fgms.freeflightsim.org/fgms_conf.html
    # https://sourceforge.net/projects/fgms/

    - name: Install compiler stuff for FGMS
      ansible.builtin.apt:
        install_recommends: true
        only_upgrade: false
        update_cache: yes
        name:
          - git
          - gcc
          - g++
          - make
          - cmake
        state: present

    - name: Create directories for FGMS compilation
      ansible.builtin.file:
        path: "{{ build_fgms_dir }}"
        state: directory
        owner: hunter
        group: hunter

    - name: Clone the FGMS repository
      become_user: hunter
      ansible.builtin.git:
        repo: https://git.code.sf.net/p/fgms/src
        depth: 1
        dest: "{{ src_fgms_dir }}"

    - name: Run cmake for FGMS
      become_user: hunter
      ansible.builtin.command: cmake {{ src_fgms_dir }}
      args:
        chdir: "{{ build_fgms_dir }}"

    - name: Run make for FGMS
      become_user: hunter
      ansible.builtin.command: make
      args:
        chdir: "{{ build_fgms_dir }}"

    - name: Run make install for FGMS
      # need to run as root
      become: yes
      ansible.builtin.command: make install
      args:
        chdir: "{{ build_fgms_dir }}"

    - name: Create the FGMS configuration file
      become_user: hunter
      ansible.builtin.copy:
        content: |
          server.name = hunterfgms
          server.port = 5000
          server.telnet_port = 0
          server.admin_port = 0
          server.playerexpires = 10
          server.logfile = {{ hunter_working_dir }}/fgms.log
          server.tracked = false
          server.daemon = true
          server.is_hub = false
          server.out_of_reach = 500
          server.max_radar_range = 2000
        dest: "{{ hunter_working_dir }}/fgms.conf"
        owner: hunter
        group: hunter

    - name: Create the FGMS systemd service file
      become: yes
      ansible.builtin.copy:
        content: |
          [Unit]
          Description=FlightGear Multiplayer daemon
          [Service]
          User=hunter
          WorkingDirectory={{ hunter_working_dir }}
          Type=forking
          ExecStart=/usr/local/sbin/fgms -c {{ hunter_working_dir }}/fgms.conf
          Restart=always
          [Install]
          WantedBy=multi-user.target
        dest: /etc/systemd/system/fgms.service

    - name: Enable the FGMS service
      become: yes
      ansible.builtin.command: systemctl enable fgms.service

    - name: Start the FGMS service
      become: yes
      ansible.builtin.command: systemctl start fgms.service

    - name: Download NanoMQ package
      ansible.builtin.get_url:
        url: https://www.emqx.com/en/downloads/nanomq/0.23.1/nanomq-0.23.1-linux-amd64.deb
        dest: /tmp/nanomq-0.23.1-linux-amd64.deb
    
    - name: Install NanoMQ package
      ansible.builtin.apt:
        deb: /tmp/nanomq-0.23.1-linux-amd64.deb

    #- name: Clone the repositories
    #  include: ansible_playbooks/clone_repositories.yml
    - name: Create directory for Hunter development
      ansible.builtin.file:
        path: "{{ repos_dir }}"
        state: directory
        owner: hunter
        group: hunter

    - name: Clone the hunter-vm repository
      become_user: hunter
      ansible.builtin.git:
        repo: https://gitlab.com/vanosten/hunter-vm.git
        depth: 1
        dest: "{{ repos_dir }}/hunter-vm"

    - name: Clone the Hunter repository
      become_user: hunter
      ansible.builtin.git:
        repo: https://gitlab.com/vanosten/hunter.git
        depth: 1
        dest: "{{ repos_dir }}/hunter"

    - name: Clone the Hunter scenarios repository
      become_user: hunter
      ansible.builtin.git:
        repo: https://gitlab.com/vanosten/hunter-scenarios.git
        depth: 1
        dest: "{{ repos_dir }}/hunter-scenarios"

    - name: Install Python and other stuff for Hunter controller and dependencies for ATC-pie
      become: yes
      ansible.builtin.apt:
        install_recommends: true
        only_upgrade: false
        update_cache: yes
        name:
          - libglib2.0-0
          - python3
          - python3-dev
          - python3-pip
          - python3-venv
          - proj-bin
          - proj-data
        state: present

    - name: Create a Python virtual env
      become_user: hunter
      ansible.builtin.command: python3 -m venv venv
      args:
        chdir: "{{ bin_dir }}"

    - name: Install libraries into the Python virtual env
      become_user: hunter
      ansible.builtin.command: "{{ bin_dir }}/venv/bin/python3 -m pip install -r requirements_controller.txt"
      args:
        chdir: "{{ repos_dir }}/hunter"

    - name: Copy the nanomq configuration file
      ansible.builtin.copy:
        src: "{{ repos_dir }}/hunter/nanomq.conf"
        dest: "{{ hunter_working_dir }}/nanomq.conf"
        owner: hunter
        group: hunter
        mode: u=rw,g=wr,o=r

    - name: Copy the Hunter run bash script over
      ansible.builtin.copy:
        src: "{{ repos_dir }}/hunter-vm/run_hunter.sh"
        dest: "{{ hunter_working_dir }}/run_hunter.sh"
        owner: hunter
        group: hunter
        mode: u=rwx,g=wr,o=r
...
