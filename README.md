# About

This repo contains documentation and infrastructure as code to run the server side of [Hunter](https://gitlab.com/vanosten/hunter>) in Microsoft Azure.

## Virtual Machine (VM)

The VM hosts 2 server processes:
* A FlightGear MultiPlayer server
* The Hunter controller process

NB: Given the used relatively small VM size and because most environment variables are not set, the Hunter controller can only drive simulated targets and no targets as FG Instances.

Both processes are "compiled" into the VM instead of using Docker images. The reasons are:
* Do not run the overhead of Docker (performance, even though not so significant)
* Reduce what needs to be downloaded at creation of the VM
* Allow updates from Git to be fetched for Hunter, such that latest features can be used without recreating the VM.
sudo

## Using the VM

You might need to do a `ssh-keygen -f "~/.ssh/known_hosts" -R "hunterctrl-vm-001.westeurope.cloudapp.azure.com"` before using `ssh` the first time, because the fingerprint changes every time the VM is recreated (the domain name of your server might differ).

The user (with sudo rights) is `hunter`.

The FGMS service is enabled and started by default as a daemon. You can stop it with `sudo systemctl stop mydocker.service`.

To start Hunter change into `hunter_workingdir`, edit `run_hunter.sh` (e.g. the name of the scenario, the domain name of the MP server or the credentials for saving state in cloud) and then issue `./run_hunter.sh`.

To stop hunter, issue `ps -e`. look for the first `python3` process and kill it.

E.g. 

```
$ ps -e
   ...
   4933 ?        00:00:00 fgms
   5334 ?        00:00:00 kworker/0:1-events
   7730 ?        00:00:00 sshd
   7787 ?        00:00:00 sshd
   7788 pts/1    00:00:00 bash
   7806 pts/0    00:00:00 python3
   7809 pts/0    00:00:22 python3
   7811 pts/0    00:00:00 python3
   ...
$ kill 7806
```

## Creating the VM and other Azure stuff

Have a look at the [docs](https://gitlab.com/vanosten/hunter-vm/-/blob/main/azure_stuff.rst?ref_type=heads).
