variable "resource_group_name" {
  type    = string
  default = "rg-hunterctrl"
}
variable "location" {
  type    = string
  default = "North Europe"
}
variable "domain_name_host_part" {
  type    = string
  default = "hunterctrl-vanosten"
}
variable "domain_name_bastion_part" {
  type    = string
  default = "hunterctrl-bastion-vanosten"
}
variable "hunter_password" {
  description = "Password for user 'hunter' in the VM"
  type        = string
  sensitive   = true
}

variable "AZ_COSMOS_ACCOUNT_URI" {
  description = "Azure Cosmos Account URI"
  type        = string
  default     = "https://hunter-nosql.documents.azure.com:443/"
}

variable "AZ_COSMOS_TENANT_ID" {
  description = "Azure Cosmos Tenant ID"
  type        = string
  sensitive   = true
}

variable "AZ_COSMOS_CLIENT_ID" {
  description = "Azure Cosmos Client ID"
  type        = string
  sensitive   = true
}

variable "AZ_COSMOS_CLIENT_SECRET" {
  description = "Azure Cosmos Client Secret"
  type        = string
  sensitive   = true
}

variable "APPLICATIONINSIGHTS_CONNECTION_STRING" {
  description = "The Azure Application Insights connection string"
  type        = string
  sensitive   = true
}