# Configure the Azure provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 4.0.1"
    }
  }

  required_version = ">= 1.9.5"
}

provider "azurerm" {
  features {}
}

locals {
  env_variables = {
    AZ_COSMOS_ACCOUNT_URI   = var.AZ_COSMOS_ACCOUNT_URI
    AZ_COSMOS_TENANT_ID     = var.AZ_COSMOS_TENANT_ID
    AZ_COSMOS_CLIENT_ID     = var.AZ_COSMOS_CLIENT_ID
    AZ_COSMOS_CLIENT_SECRET = var.AZ_COSMOS_CLIENT_SECRET
    APPLICATIONINSIGHTS_CONNECTION_STRING = var.APPLICATIONINSIGHTS_CONNECTION_STRING
    OTEL_SERVICE_NAME       = "hunter-ctrl"
  }
}

resource "azurerm_resource_group" "rg-hunterctrl" {
  name     = var.resource_group_name
  location = var.location
}

resource "azurerm_virtual_network" "vnet-hunterctrl" {
  name                = "vnet-hunterctrl"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.rg-hunterctrl.location
  resource_group_name = azurerm_resource_group.rg-hunterctrl.name
}

resource "azurerm_subnet" "snet-hunterctrl-default" {
  name                 = "snet-hunterctrl-default"
  resource_group_name  = azurerm_resource_group.rg-hunterctrl.name
  virtual_network_name = azurerm_virtual_network.vnet-hunterctrl.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_public_ip" "pip-hunterctrl-vm-001" {
  name                  = "pip-hunterctrl-vm-001"
  resource_group_name   = azurerm_resource_group.rg-hunterctrl.name
  location              = azurerm_resource_group.rg-hunterctrl.location
  allocation_method     = "Static"
  sku                   = "Basic"
  sku_tier              = "Regional"
  domain_name_label     = var.domain_name_host_part
}

resource "azurerm_network_interface" "nic-hunterctrl-vm-001" {
  name                = "nic-hunterctrl-vm-001"
  location            = azurerm_resource_group.rg-hunterctrl.location
  resource_group_name = azurerm_resource_group.rg-hunterctrl.name

  ip_configuration {
    name                          = "default"
    subnet_id                     = azurerm_subnet.snet-hunterctrl-default.id
    private_ip_address_allocation = "Dynamic"
    primary                       = true
    public_ip_address_id          = azurerm_public_ip.pip-hunterctrl-vm-001.id
  }
}

resource "azurerm_network_security_group" "nsg-hunterctrl-vm" {
  name                = "nsg-hunterctrl-vm"
  location            = azurerm_resource_group.rg-hunterctrl.location
  resource_group_name = azurerm_resource_group.rg-hunterctrl.name

  security_rule {
    name                       = "SSH"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "RDP"
    priority                   = 200
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "3389"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "FlightGear-MP-in"
    priority                   = 300
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_range     = "5000-5299"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_subnet_network_security_group_association" "nsg-hunterctrl-vm-associate" {
  subnet_id                 = azurerm_subnet.snet-hunterctrl-default.id
  network_security_group_id = azurerm_network_security_group.nsg-hunterctrl-vm.id
}

resource "azurerm_linux_virtual_machine" "vm-hunterctrl-vm-001" {
  name                            = "vm-hunterctrl-vm-001"
  resource_group_name             = azurerm_resource_group.rg-hunterctrl.name
  location                        = azurerm_resource_group.rg-hunterctrl.location
  size                            = "Standard_D2s_v3"
  admin_username                  = "hunter"
  admin_password                  = var.hunter_password
  disable_password_authentication = false
  encryption_at_host_enabled      = false
  network_interface_ids           = [
    azurerm_network_interface.nic-hunterctrl-vm-001.id,
  ]

  custom_data                     = base64encode(templatefile("${path.module}/vm-cloud-init.yml", { env_vars = local.env_variables }))

# ssh key does not play nicely with KRDC
#  admin_ssh_key {
#    username   = "adminuser"
#    public_key = file("~/.ssh/hunter-vm-001_id_rsa.pub")
#}

  os_disk {
    name                 = "osdisk-hunterctrl-vm-001"
    caching              = "ReadOnly"
    storage_account_type = "StandardSSD_LRS"
  }

  source_image_reference {
    publisher = "canonical"
    offer     = "ubuntu-24_04-lts"
    sku       = "server"
    version   = "latest"
  }
}

# ------------
# Bastion host
# ------------

# The Subnet used for the Bastion Host must have the name AzureBastionSubnet and the subnet mask must be at least a /26.
/* resource "azurerm_subnet" "AzureBastionSubnet" {
  name                 = "AzureBastionSubnet"
  resource_group_name  = azurerm_resource_group.rg-hunterctrl.name
  virtual_network_name = azurerm_virtual_network.vnet-hunterctrl.name
  address_prefixes     = ["10.0.2.0/24"]
}


resource "azurerm_public_ip" "pip-hunterctrl-bastion" {
  name                  = "pip-hunterctrl-bastion"
  resource_group_name   = azurerm_resource_group.rg-hunterctrl.name
  location              = azurerm_resource_group.rg-hunterctrl.location
  allocation_method     = "Static"
  sku                   = "Basic"
  sku_tier              = "Regional"
  domain_name_label     = var.domain_name_bastion_part
}


resource "azurerm_bastion_host" "host-hunterctrl-bastion" {
  name                = "host-hunterctrl-bastion"
  location            = azurerm_resource_group.rg-hunterctrl.location
  resource_group_name = azurerm_resource_group.rg-hunterctrl.name
  sku                 = "Developer"
  virtual_network_id  = azurerm_virtual_network.vnet-hunterctrl.id
}
 */